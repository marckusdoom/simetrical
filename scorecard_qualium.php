<?php  
/*
* Template Name: Scorecard_qualium
*/
?>
<?php get_header() ?>
<div class="productosSeccion2">
	<section>
				<img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion1.jpg">
		<div class="over">
			<a href="#secdos">
  			 <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>SCORECARD: <span> EL SECRETO DEL<br>
              EMPRESARIO EXITOSO </span></h2>
             <span class="line"></span>	
  			 <h6>Sabemos que la vida de un ejecutivo de alto nivel pude ser algo agitada, por eso diseñamos<br>
  			 Scorecard, un tablero de control que te permite visualizar diariamente la información completa de tu<br>
  			 empresa. Todos los días recibirás el tablero de control con las métricas actualizadas de tu<br>
  			 negocio para una toma de desiciones más ágil y oportuna, que te permitirá maximizar los recursos y<br>
  			 optimizar los procesos, traduciéndose en un incremento de productividad.</h6>
			    <div class="small-12  medium-7 medium-offset-3 large-5 large-offset-4 columns ">
			         <?php 
					echo do_shortcode('[wysija_form id="1"]');
					 ?> 
			    </div>
			</div>
		</div>
	</section>
	<section id="secdos">
	    <h3>ÓPTICAS DE NEGOCIOS ACTUALIZADAS <br>
	        DONDE QUIERA QUE ESTÉS.
	    </h3>
	    <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/imagen1.png" class="showimage">
		<ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
            <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/doc.png" class="image">
		  	<h5>Información actualizada</h5>
		  	   <h6>Nuestro sistema automatizado recopilará tu información <br>
		  	   	durante la noche, mientras descansas. Cuando <br>
		  	    despierte tu información estará actualizada en todos <br>
		  	     tus dispositivos.</h6>
		  </li>
		  <li>
            <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/grafica.png" class="image">
		  	<h5>Escoge tus indicadores</h5>
              <h6>Demasiada información puede resultar abrumadora, pero <br>
		  	   	  Scorecard te permite seleccionar los indicadores que más te<br>
		  	      interesan para tenerlos en tu tablero de control principal.<br>
		  	      Podrás acceder desde PC, Mac o iPad.</h6>
		  </li>
		  <li>
		  <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/like.png" class="image">
		  	<h5>Toma mejores decisiones</h5>
              <h6>La información se puede analizar de manera general, por <br>
              	  unidad de negocio, departamento, indicador y detalle de las <br>
                  transacciones, lo que permite estar al tanto en todo <br>
                  momento, en cualquier parte del mundo.
                  </h6>
		  </li>
		</ul>
	</section>
	<section>
		<?php  $args = array(
							'post_type'   => 'ScoreTestimonios',
							'order'=>'ASC'
		 				);
					$query = new WP_Query($args); ?> 		
		<div class="owl-carousel">
				<?php while ($query->have_posts()): $query->the_post(); ?>     
				<div class="item">
					     <h3><?= get_the_content(get_the_ID()); ?></h2>
					     <h4><?= get_the_title(get_the_ID()); ?></h3>
				  </div>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>	
	   </div>
	</section>
	<section>
         <h4>TODOS NECESITAN SER MÁS PRODUCTIVOS</h4>
         <h6>Trabajamos con todo tipo de industrias,no importa si eres un <br>
         	hospital,uin gobierno o una universidad, Scorecard te ayudará <br>
         	a tomar mejores decisíones</h6>
         <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
		  <li>
		  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen1.png">
		  	<h3>Conseccionarias <br>
		  	 Automotrices</h3>
		  </li>
		  <li>
		  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen2.png">
		  	<h3>Cadenas <br>
		  		Restaurantes</h3>
		  </li>
		   <li>
		   	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion2_imagen3.png">
		  	<h3>Sitios de <br>
		  	 Ecommence</h3>
		  </li>
		</ul>
	</section>
	<section>
    <h3>CONVIERTETE EN UN EMPRESARIO <br>
    	REALMENTE EXITOSO</h3>
    	<?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos 3 datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>
<?php get_footer() ?>