<?php  
/*
* Template Name: Custumer_pulse_qualium
*/
?>
<?php get_header() ?>
<div class="productosSeccion4">
	<section>
		  <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4.png">
		<div class="over">
			<a href="#secdos">
	  			<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>CUSTOMER PULSE<br>
             <span>
              UN CANAL DIRECTO CON SU CLIENTES
             </span>
           </h2>
             <span class="line"></span>	
  			 <h6>El customer pulse es una encuesta de salida que se realiza a todos tus clientes con<br>
  			 tan solo 3 preguntas. Ésta se realiza en un dispositivo electrónico en sitio y en 10<br>
  			 segundos se conoce la experiencia del 100% de los clientes.
  			 </h6>
			    <div class="small-12  medium-7 medium-offset-3 large-5 large-offset-4 columns ">
			       <?php 
					echo do_shortcode('[wysija_form id="1"]');
					 ?> 
			        <!-- <input type="text" placeholder="correo electronico" />
			        <input id="send" type="button" value="ENVIAR"/> -->
			    </div>
  			</div>
		</div>
	</section>
	<section id="secdos">
    <h3>
    	¿QUIERE CLIENTES FELICES? ANALICE DIARIAMENTE <BR> LA 
        SATISFACCIÓN DE SUS CLIENTES
    </h3>
    <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4_imagen1.png">
    <div class="over">
       <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
			  <li>
			  	<h6>Gestión de crisis</h6>
			    <h6 class="sub">No dejes que tu cliente se vaya molesto. Convierte una mala experiencia 
			     en una buena experiencia de atención al cliente.</h6>
			  </li>
			   <li><h6>Información en tiempo real</h6>
			    <h6 class="sub">Olvídate de capturar datos. La información quedará registrada automáticamente
			    	y estará disponible para ti de inmediato.</h6>
			  </li>
			  <li><h6>Integración con Scorecard</h6>
			  	<h6 class="sub">Enlaza tu Customer Pulse con <a class="link" href="<?= get_the_permalink(50) ?>">Scorecard</a> y agrega este indicador a tu tablero de control.
			  	</h6>
			  </li>
		</ul>
    </div>
	</section>
	<section>
        <div class="over">
        <h3>"Una mente llena de dudas no <br>
        	se puede concentrar en una victoria"</h3>
        <h4>-Arthur Golden</h4>
        </div>
	</section>
	<section>
    <h3>TODOS NECESITAN SABER LO QUE <BR>
        SUS CLIENTES QUIEREN.
    </h3>
    <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
	  <li>
	  	<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4car.jpg">
	  	<h4 id="custumer">Conseccionarias <br>
        Automotrices</h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion4ham.jpg">
		<h4 id="custumer">Cadenas <br>
        Restaurantes</h4>
	  </li>
	  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccionbol.jpg">

	  	<h4 id="custumer">
	  		Sitios de <br>
    Ecommence</h4></li>
	</ul>
	</section>
    <section>
    <h3>QUIERO CONOCER MEJOR <br>
    	A MIS EMPLEADOS</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos 3 datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>
	
<?php get_footer() ?>