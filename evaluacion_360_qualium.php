<?php  
/*
* Template Name: Evaluacion_360_qualium
*/
?>
<?php get_header() ?>

<div class="productosSeccion3">
	<section>
  <img class="fondo" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion3.jpg">
		<div class="over">
			<a href="#secdos">
	  		<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			</a>
			<div id="container_text">
            <h2>EVALUACION DE 360 GRADOS:<br>
              <span>EL EMPLEADO CORRECTO EN <br>
              EL PUESTO CORRECTO</span>
           </h2>
             <span class="line"></span>	
  			 <h6>La evaluación de 360 Grados, es un método efectivo para obtener la infomación sobre los resultados<br>
  			 de desempeño y los patrones de comportamiento en los gerentes y personal clave de la empresa. Si<br>
  			 estas por hacer movimientos importantes de recursos humanos o quieres saber si un empleado está<br>
  			 la posición adecuada, este producto es para ti.<br>
  			 </h6>
  			 <div class="small-12 medium-7 medium-offset-3 large-5 large-offset-4 columns ">
			       <?php 
					echo do_shortcode('[wysija_form id="1"]');
					 ?> 
			        <!-- <input type="text" placeholder="correo electronico" />
			        <input id="send" type="button" value="ENVIAR"/> -->
			    </div>
  			</div>
		</div>
	</section>
	<section id="secdos">
           <h3 class="hide-for-small-only">TOMA MEJORES DECISIONES</h3>
           <div class="over">
            <ul class="small-block-grid-1 medium-block-grid-3 large-block-grid-3">
               <h3 class="show-for-small-only">TOMA MEJORES DECISIONES</h3>
			  <li>
			  	<h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Información invaluable</h6>
			    <h6 class="sub">Conocer a fondo las habilidades y debilidades de tu <br>
			    	equipo es una ventaja que todo directivo necesita tener. <br>
			    	Mientras más información, mejores desiciones.</h6></li>
			  <li><h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Fácil aplicación</h6>
			    <h6 class="sub">Sus empleados recibirán un correo electróníco con una liga <br>
			    que deberán seguir para realizar la evaluación, que no dura	<br>
			    mas de 10 minutos.</h6></li>
			  <li><h6><img class="check" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Evaluados por todos</h6>
			  	<h6 class="sub">Pará la obtención de resultados certeros, los gerentes y <br>
			  		personal clave son evaluados por todo su entorno: jefes, <br>
			  		pares y subordinados.</h6></li>
			</ul>
           </div>
	</section>
	<section>
	<h4>AUTOEVALUACION EN <BR>
		11 COMPETENCIAS CLAVE</h4>
	<div class="row">
		<div class="small-12 medium-6 large-6 columns">
		    <ul class="small-block-grid-1">
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Aprendizaje</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Compromiso</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Comunicación</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Disponibilidad</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Liderazgo</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Manejo del conflicto</li>
			</ul>
		</div>
		<div class="small-12 medium-6 large-6 columns">
		    <ul class="small-block-grid-1">
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Orientación</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Orientación al servicio</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Proactividad</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Solución de problemas</li>
			  <li><img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/check.png">Trabajo en equipo</li>
			</ul>
		</div>
	</div>
	</section>
	<section>
               <div class="over">
               <h2>"El camino al éxito es medir"</h2>
               </div>
	</section>
	<section>
	   <div class="row">
          <div class="small-5 medium-7 large-7 columns image">
          	       <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/seccion3_imagen3.png">
          </div>
          <div class="small-7 medium-5 large-5 columns text">
          	       <h3>Analiza los resultados</h3>
          	       <h6>Los resultados son analizados en una <br>
          	       	herramienta sencilla que le permite identificar, <br>
          	       	a nivel de cada gerente en particular o del <br>
          	        conjunto gerencial, las fortalezas y debilidades, <br>
          	        estableciendo planes de acción dirigidos y personalizados.
          	     </h6>
          </div>
	   </div>
	</section>
	<section>
    <h3>QUIERO CONOCER MEJOR <br>
    	A MIS EMPLEADOS</h3>
    <?php get_template_part( '/simetrical_qualium/form', 'single' ); ?>
    <h6 class="sub">Sólo necesitamos 3 datos y uno de nuestros asesores especializados, <br>
        se comunicará contigo en menos de 24 horas.
    </h6>
    </section>
</div>

<?php get_footer() ?>