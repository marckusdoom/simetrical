<?php 
/*
* Template Name: Home_qualium
*/
 ?>
<?php get_header() ?>
<div class="productos">
	<section id="seccion1">
		<video id="video" loop="loop" autoplay="autoplay">
			 <source type="video/mp4" src="<?php echo site_url() ?>/wp-content/uploads/2015/01/Scorecard319202.mp4">
			 <source type="video/webm" src="<?php echo site_url() ?>/wp-content/uploads/2015/01/Scorecard3_1920.webmhd.webm"> 
			</video>
		<div class="over">

		    <a href="#secdos">
			<img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png" class="arrow">
			<a>
			<div id="container_text">
            <h2>PARA TOMAR LAS MEJORES DECIONES,<BR>
             UN EMPRESARIO REQUIERE SABER DIARIAMENTE <BR>
             LO QUE OCURRE EN SU EMPRESA<br>
             <span class="line"></span>	
             <div>
  			 <h6>Creemos ﬁrmemente que ayudando a los dueños de negocios como tu a tener<br>
  			 ópticas actualizadas, participamos en mejorar la productividad y rentabilidad de tu <br>
  			 compañía. De esta manera, ponemos nuestro grano de arena para crecer tu <br>
  			 negocio y generar fuentes de empleo, ayudándote a tomar decisiones precisas y en el<br>
  			 momento adecuado.</h6>
  			 </div>
  			 </div>
		</div>
	</section>
	<section id="secdos">
             <h3>HAY MUCHO QUE PODEMOS HACER POR TI</h3>
             <h6>Contamos con diversos productos que te ayudarán a monitorear los <br>
             	indicadores de negocio más importantes para ti</h6>
             <h6>Nuestro compromiso: la optimización y maximizacion de tu compañía, pues nos <br>
             	apasiona generar métricas, ayudandote a crecer.</h6>
              <div class="container_carru">
              <a id="arrowrigth">
              <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png?>" class="arrowrigth">
			  </a>
              <a id="arrowleft">
			   <img src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/arrow.png?>" class="arrowleft">
			  </a> 
			<?php $args = array(
				'parent' => get_the_ID(),
			); 
			$pages = get_pages($args); 
			?>
			<?php $conta = 1; ?>
			<?php foreach ($pages as $key => $value): ?>
                 <?php if ($conta == 1) {
                 	$clase ="img_left";
                 }elseif ($conta == 2) {
                 	$clase ="img_center";
                 }else{
                 	$clase ="img_rigth";
                 } ?>
				 <img data-img="<?= $conta?>" class="<?= $clase ?>" draggable="false" id="image" src="<?= wp_get_attachment_url(get_post_thumbnail_id($value->ID)) ?>"> 
			<?php  $conta++; ?>
			<?php endforeach ?>
			</div>
			 <?php $conta = 1; ?>
			 <?php foreach ($pages as $key => $value): ?>
			 <div id="link<?= $conta?>" class="product">
              <h3 id="custumer"><?php echo $value->post_title ?></h3>
              <h6><?=  $value->post_content ?></h6>
               <a href="<?= get_the_permalink($value->ID) ?>" class="Btn-Green button" >Más información</a>
     		</div>
             <?php  $conta++; ?>
			<?php endforeach ?>
			<?php wp_reset_query(); ?>

	</section>
	<section class="text-center">

		<div class="over">
			<?php 
					$args = array(
							'post_type'   => 'hometestimonios',
							'order'=>'ASC'
		 				);
					$query = new WP_Query($args); 
				?>
					<?php while ($query->have_posts()): $query->the_post(); ?>
					<?= get_the_title(get_the_ID()); ?>
					<?php endwhile; ?>
					<?php wp_reset_query(); ?>	
			<!-- <div class="owl-carousel">
			    <div class="item">
				     <h3>"El riesgo viene de no saber<br>
				     lo que estás haciendo, todos los <br>
				     empresarios requieren opticas"</h2>
				     <h4>-Warren Buffett</h3>
		        </div>
                <div class="item">
				     <h3>"El riesgo viene de no saber<br>
				     lo que estás haciendo, todos los <br>
				     empresarios requieren opticas"</h2>
				     <h4>-Warren Buffett</h3>
		        </div>
			</div> -->
		</div>
	</section>
	<section>
            <h3>MARCAS QUE HAN CONFIADO EN NOSOTROS</h3>
            <ul class="small-block-grid-1 medium-block-grid-5 large-block-grid-5">
			  <li><img id="logo_clientes" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/honda.png ?>"></li>
			  <li><img id="logo_clientes" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/nissan.png ?>"></li>
			  <li><img id="logo_clientes" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/toyota.png ?>"></li>
			  <li><img id="logo_clientes" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/susuki.png ?>"></li>
			  <li><img id="logo_clientes" src="<?php echo get_template_directory_uri() ?>/simetrical_qualium/assets/volkswagen.png ?>"></li>
			</ul>
	</section>
	<section>
           <h3>¿QUIERES TENER OPTICA DIARIA?</h3>
           <h6>Déjanos tus datos y un asesor se comunicara en menos <br>
            de 24 horas</h6>
            <fieldset>
            <div  class="small-10 small-offset-1 medium-6 medium-offset-3  large-offset-4  large-4 columns">
			<?php 
			echo do_shortcode('[contact-form-7 id="1357" title="form_climaorganizacional"]');
			 ?>
			</div>
           </fieldset>
	</section>
</div>
<?php get_footer() ?>